variable "global" {
    type = map(string)
    default = {
        Region      = "us-west-2"
        Application = "K8s"
        Environment = "dev"
    }
}

variable "route53" {
    type = map(string)
    default = {
        subdomain   = "k8s"
        domain      = "aztek.io"
    }
}

locals {
    private_key = "${path.module}/../k8s.pem"
    public_key  = "${path.module}/../k8s.pub"
}
