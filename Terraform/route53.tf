data "aws_route53_zone" "selected" {
    name = var.route53["domain"]
}

resource "aws_route53_record" "k8s" {
    zone_id = data.aws_route53_zone.selected.zone_id
    name    = "${var.route53["subdomain"]}.${var.route53["domain"]}"
    type    = "NS"
    ttl     = "3600"
    records = aws_route53_zone.k8s.name_servers
}

resource "aws_route53_zone" "k8s" {
    name = "${var.global["Environment"]}.${var.route53["subdomain"]}.${var.route53["domain"]}"
}
