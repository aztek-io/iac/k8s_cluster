resource "aws_s3_bucket" "k8s" {
    bucket          = "${var.global["Environment"]}.${var.route53["subdomain"]}.${var.route53["domain"]}"
    force_destroy   = false

    tags = {
        Application = var.global["Application"]
        Environment = var.global["Environment"]
    }
}

