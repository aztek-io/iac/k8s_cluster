resource "tls_private_key" "ssh" {
    algorithm   = "RSA"
    rsa_bits    = 4096
}

resource "local_file" "public_key" {
    sensitive_content = tls_private_key.ssh.public_key_pem
    filename          = local.public_key
}

resource "local_file" "private_key" {
    sensitive_content = tls_private_key.ssh.private_key_pem
    filename          = local.private_key
}

resource "null_resource" "key_conversion" {
    depends_on  = [
        local_file.public_key
    ]

    provisioner "local-exec" {
        command = "ssh-keygen -f ${path.module}/../k8s.pub -i -m PKCS8 > ${path.module}/../k8s_converted.pub"
    }
}

resource "aws_s3_bucket_object" "ssh_private_key" {
    bucket      = aws_s3_bucket.k8s.id
    key         = "k8s.pem"
    source      = local.private_key
}

resource "aws_s3_bucket_object" "ssh_public_key" {
    bucket      = aws_s3_bucket.k8s.id
    key         = "k8s.pub"
    source      = local.public_key
}

resource "aws_s3_bucket_object" "ssh_public_key_converted" {
    depends_on  = [
        null_resource.key_conversion
    ]

    bucket      = aws_s3_bucket.k8s.id
    key         = "k8s_converted.pub"
    source      = "${path.module}/../k8s_converted.pub"
}

