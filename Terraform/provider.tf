provider "aws" {
    region  = var.global["Region"]
    version = ">= 2.0"
}
