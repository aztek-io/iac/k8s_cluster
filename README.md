# Documentation

## Setup steps:
1. Set `KOPS_STATE_STORE` variable.
2. Change cluster name in the following places (Will refactor this soon).
    - Terraform/remote_state.tf
    - .gitlab-ci.yml
    - Kops/ (all files).
3. Change datadaog account in Kops/nodes-us-west-2a.yml
